package carPolicy;

public class CarPolicyTest {

	public static void main(String[] args) {
		
		CarPolicy carPolicy1 = new CarPolicy(); // Leeres Objekt 
		CarPolicy carPolicy2 = new CarPolicy(2,"FD");
		CarPolicy carPolicy3 = new CarPolicy(2,"OP");
		
	//	policyPrintMethod(carPolicy1);
		policyPrintMethod(carPolicy2);
		policyPrintMethod(carPolicy3);
		

		
		

	}
	
	public static void policyPrintMethod (CarPolicy policy) {
		System.out.println("Das Auto :");
		System.out.printf("Hier es ist! Make and Model %s, %d, car %s made %n",
				policy.getMarkeUndModell(), policy.getNummerSchield(), 
				policy.isNoMakeModel() ? "is": "is not");
	}

}
