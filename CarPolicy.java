package carPolicy;

public class CarPolicy {
	
	//variablen für eine Klasse - innerhalb die klasse schweife klammern
	private int nummerSchield;
	private String markeUndModell;

	//Konstructor
	public CarPolicy() { 
		//leer Konstructor
	}
	
	public CarPolicy(int nummer, String marke) {
		this.markeUndModell = marke;
		this.nummerSchield = nummer;
		
	}

	public int getNummerSchield() {
		return nummerSchield;
	}

	public void setNummerSchield(int nummerSchield) {
		this.nummerSchield = nummerSchield;
	}

	public String getMarkeUndModell() {
		return markeUndModell;
	}

	public void setMarkeUndModell(String markeUndModell) {
		this.markeUndModell = markeUndModell;
	}

	public boolean isNoMakeModel() {
		boolean noMake;
		switch (getMarkeUndModell()) {
			case "TY": case "OP": case "FD": 
				noMake = true;
				break;
			default:
				noMake = false;
				break;
		}
		return noMake;
	}
}
